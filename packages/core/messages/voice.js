'use strict'

const Message = require('./message')

const TYPE = 'voice'

/**
 * Voice 语音消息
 */
module.exports = class Voice extends Message {
  constructor ({mediaID}) {
    super()
    this.props = ['mediaID']
    this.aliases = {
      'mediaID': 'media_id'
    }
    this.type = TYPE
    this.mediaID = mediaID
  }

  toXMLObject () {
    return {
      MediaId: this.mediaID
    }
  }
}
