'use strict'

const Message = require('./message')

module.exports = class Template extends Message {
  constructor () {
    super()
    this.type = 'template'

    this.props = ['templateID', 'url', 'data', 'color']
    this.aliases = {
      'templateID': 'template_id'
    }
  }
}
