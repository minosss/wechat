'use strict'

const Message = require('./message')

const TYPE = 'image'

/**
 * Image 图片消息
 */
module.exports = class Image extends Message {
  constructor ({mediaID}) {
    super()
    this.props = ['mediaID']
    this.aliases = {
      'mediaID': 'media_id'
    }
    this.type = TYPE
    this.mediaID = mediaID
  }

  toXMLObject () {
    return {
      MediaId: this.mediaID
    }
  }
}

/*
// XML
<xml>
    <ToUserName>
        <![CDATA[toUser]]>
    </ToUserName>
    <FromUserName>
        <![CDATA[fromUser]]>
    </FromUserName>
    <CreateTime>1348831860</CreateTime>
    <MsgType>
        <![CDATA[image]]>
    </MsgType>
    <PicUrl>
        <![CDATA[this is a url]]>
    </PicUrl>
    <MediaId>
        <![CDATA[media_id]]>
    </MediaId>
    <MsgId>1234567890123456</MsgId>
</xml>
*/
