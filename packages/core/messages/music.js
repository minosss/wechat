'use strict'

const Message = require('./message')

const TYPE = 'music'

/**
 * 音乐消息
 */
module.exports = class Music extends Message {
  constructor ({title, description, musicURL, hqMusicURL, thumbMediaID}) {
    super()
    this.props = ['title', 'description', 'musicURL', 'hqMusicURL', 'thumbMediaID']

    this.type = TYPE
    this.title = title
    this.description = description
    this.musicURL = musicURL
    this.hqMusicURL = hqMusicURL
    this.thumbMediaID = thumbMediaID
  }

  toXMLObject () {
    return {
      ThumbMediaId: this.thumbMediaID,
      Title: this.title,
      Description: this.description,
      MusicURL: this.musicURL,
      HQMusicUrl: this.hqMusicURL
    }
  }
}
