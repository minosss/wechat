'use strict'

const Message = require('./message')

const TYPE = 'text'

/**
 * Text 文本消息
 */
module.exports = class Text extends Message {
  constructor ({content}) {
    super()
    this.props = ['content']

    this.type = TYPE
    this.content = content
  }

  toXMLObject () {
    return {
      Content: this.content
    }
  }
}
