'use strict'

const { buildXMLString } = require('../utils')

class Message {
  constructor () {
    this.props = []
    this.aliases = {}
  }

  /**
   * propsToObject
   */
  propsToObject (data = {}, aliases = {}) {
    for (const p of this.props) {
      if (this[p] !== undefined && this[p] !== null) {
        data[aliases[p] || p] = this[p]
      }
      data[aliases[p] || p] = this[p]
    }
    return data
  }

  /**
   * transformForJsonRequestWithoutType
   */
  transformForJsonRequestWithoutType (appends = {}) {
    return this.transformForJsonRequest(appends, false)
  }

  /**
   * transformForJsonRequest
   */
  transformForJsonRequest (appends = {}, withType = true) {
    if (!withType) {
      return Object.assign(this.propsToObject({}, this.aliases), appends)
    }
    let data = Object.assign({msgtype: this.type}, appends)
    data[this.type] = Object.assign(data[this.type] || {}, this.propsToObject({}, this.aliases))
    return data
  }

  /**
   * transformToXmlString 换成微信消息格式XML字符串
   */
  transformToXmlString (appends) {
    const data = Object.assign({
      MsgType: this.type
    }, this.toXMLObject(), appends)
    return buildXMLString(data)
  }

  /**
   * 获取微信属性格式
   */
  toXMLObject () {
    throw new Error('')
  }
}

module.exports = Message
