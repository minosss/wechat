'use strict'

const Message = require('./message')

const TYPE = 'mpnews'

/**
 * Article 图文消息
 */
module.exports = class Article extends Message {
  constructor () {
    super()
    this.type = TYPE

    this.props = ['title', 'mediaID', 'showCover', 'author', 'digest', 'content', 'url', 'sourceURL', 'canComment', 'onlyFansCanComment']
    this.aliases = {
      'mediaID': 'media_id',
      'showCover': 'show_cover_pic',
      'sourceURL': 'content_source_url',
      'canComment': 'need_open_comment',
      'onlyFansCanComment': 'only_fans_can_comment'
    }
  }
}
