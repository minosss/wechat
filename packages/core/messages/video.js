'use strict'

const Message = require('./message')

const TYPE = 'video'

/**
 * Video 视频消息
 */
module.exports = class Video extends Message {
  constructor ({mediaID, title = '', description = ''}) {
    super()
    this.props = ['mediaID', 'title', 'description']
    this.aliases = {
      'mediaID': 'media_id'
    }
    this.type = TYPE
    this.mediaID = mediaID
    this.title = title
    this.description = description
  }

  toXMLObject () {
    return {
      MediaId: this.mediaID,
      Title: this.title,
      Description: this.description
    }
  }
}
