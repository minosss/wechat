'use strict'

const Message = require('./message')

const TYPE = 'transfer_customer_service'

/**
 * 消息转发到客服
 */
module.exports = class Transfer extends Message {
  constructor () {
    super()
    this.props = ['account']
    this.type = TYPE
  }

  toXMLObject () {
    if (this.account) {
      // 消息转发到指定客服
      return {
        TransInfo: {
          KfAccount: this.account
        }
      }
    }
    return {}
  }
}
