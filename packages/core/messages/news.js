'use strict'

const Message = require('./message')

const TYPE = 'news'

/**
 * News 图文消息
 */
const News = module.exports = class News extends Message {
  constructor ({items}) {
    super()
    this.props = ['items']

    this.type = TYPE
    this.items = items
  }

  toXMLObject () {
    // max 8
    const items = this.items.map(item => item.toXMLObject())
    return {
      ArticleCount: items.length,
      Articles: items
    }
  }
}

class Item extends Message {
  constructor ({title, description, url, picURL}) {
    super()
    this.props = ['title', 'description', 'url', 'picURL']
    this.title = title
    this.description = description
    this.url = url
    this.picURL = picURL
  }

  toXMLObject () {
    return {
      Title: this.title,
      Description: this.description,
      Url: this.url,
      PicUrl: this.picURL
    }
  }
}

News.Item = Item

/*
<xml>
    <ToUserName>
        <![CDATA[toUser]]>
    </ToUserName>
    <FromUserName>
        <![CDATA[fromUser]]>
    </FromUserName>
    <CreateTime>12345678</CreateTime>
    <MsgType>
        <![CDATA[news]]>
    </MsgType>
    <ArticleCount>2</ArticleCount>
    <Articles>
        <item>
            <Title>
                <![CDATA[title1]]>
            </Title>
            <Description>
                <![CDATA[description1]]>
            </Description>
            <PicUrl>
                <![CDATA[picurl]]>
            </PicUrl>
            <Url>
                <![CDATA[url]]>
            </Url>
        </item>
        <item>
            <Title>
                <![CDATA[title]]>
            </Title>
            <Description>
                <![CDATA[description]]>
            </Description>
            <PicUrl>
                <![CDATA[picurl]]>
            </PicUrl>
            <Url>
                <![CDATA[url]]>
            </Url>
        </item>
    </Articles>
</xml>
*/
