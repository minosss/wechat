'use strict'

const Provider = require('./provider')

/**
 * media 临时素材
 * 媒体文件在微信后台保存时间为3天，即3天后media_id失效
 *
 * - 图片 (image) 2M 支持 JPG
 * - 语音 (voice) 2M 长度不超过 60s 支持 AMR/MP3
 * - 视频 (video) 10M 支持 MP4
 * - 缩略图 (thumb) 64KB 支持 JPG
 */
module.exports = class Media extends Provider {
  constructor (app, accessToken) {
    super(app, accessToken)
    this.allowTypes = ['image', 'voice', 'video', 'thumb']
  }

  async uploadImage (path) {
    return await this._upload('image', path)
  }

  async uploadVoice (path) {
    return await this._upload('voice', path)
  }

  async uploadVideo (path) {
    return await this._upload('video', path)
  }

  async uploadThumb (path) {
    return await this._upload('thumb', path)
  }

  async _upload (type, media) {
    if (!this.allowTypes.includes(type)) {
      throw new Error(`不支持媒体类型: ${type}`)
    }
    return await this.httpUpload('cgi-bin/media/upload', {media}, {type})
  }

  /**
   * get 获取媒体素材
   * 直接返回request方法，可以使用pipe返回到客户端
   */
  async get (mediaID) {
    // FIXME: 需要根据情况返回一个流或者json
    return await this.requestRaw('cgi-bin/media/get', 'GET', {media_id: mediaID})
  }
}
