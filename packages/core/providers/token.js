'use strict'

const crypto = require('crypto')
const Provider = require('./provider')

module.exports = class Token extends Provider {
  constructor (app, accessToken) {
    super(app, accessToken)
    this.cachePrefix = 'wechat.cache.access_token.'
    this._cacheKey = null
  }

  get cacheKey () {
    if (!this._cacheKey) {
      const md5 = crypto.createHash('md5')
      this._cacheKey = this.cachePrefix + md5.update(JSON.stringify(this.credential)).digest('hex')
    }
    return this._cacheKey
  }

  get credential () {
    throw new Error('这个方法必须由子类实现')
  }

  /**
   * 获取access token
   * https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=SECRET
   *
   * @param {boolean} refresh
   * @return {string}
   */
  async getToken (refresh = false) {
    // cache
    if (!refresh && await this.cache.has(this.cacheKey)) {
      console.log('读取缓存access_token')
      const token = this.cache.get(this.cacheKey)
      return token
    }

    console.log('缓存过期，重新获取access_token')
    // refresh
    const qs = this.credential
    // ---
    const result = await this.httpClient({
      method: 'GET',
      url: `${this.baseUrl}cgi-bin/token`,
      json: true,
      qs
    })
    // is ok
    if (result.access_token) {
      console.log('设置缓存', this.cacheKey, result)
      // const { data } = result
      this.cache.set(this.cacheKey, result.access_token, result.expires_in - 500)
      return result.access_token
    }

    throw new Error('获取 access_token 失败')
  }

  /**
   * 强制刷新access token
   *
   * @return {self}
   */
  refresh () {
    this.getToken(true)
    return this
  }
}
