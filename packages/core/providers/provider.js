'use strict'

const request = require('../request')
const fs = require('fs')

// TODO: request重试
module.exports = class Provider {
  constructor (app, accessToken = null) {
    this._cache = null
    this.app = app
    this.accessToken = accessToken || this.app['access-token']
    this.baseUrl = 'https://api.weixin.qq.com/'
    this.httpClient = request
  }

  async requestRaw (url, method = 'GET', options = {}) {
    const accessToken = await this.accessToken.getToken()
    options.qs.access_token = accessToken

    options = Object.assign({
      baseUrl: this.baseUrl,
      timeout: 5000 // 5s
    }, options, {url, method})

    return this.httpClient(options, false)
  }

  async request (url, method = 'GET', options = {}) {
    console.log('[%s] %s', method, url)

    const accessToken = await this.accessToken.getToken()
    options.qs.access_token = accessToken

    options = Object.assign({
      baseUrl: this.baseUrl,
      json: true,
      timeout: 5000 // 5s
    }, options, {url, method})

    console.dir(options)

    try {
      const res = await this.httpClient(options)
      console.log('[response] --- ')
      console.dir(res)
      return res
    } catch (err) {
      return {
        message: `err: ${err.message}`
      }
    }
  }

  async httpGet (url, qs = {}) {
    const res = await this.request(url, 'GET', {qs})
    return res
  }

  async httpPostJson (url, body = {}, qs = {}) {
    const res = await this.request(url, 'POST', {body, qs})
    return res
  }

  async httpUpload (url, files = {}, qs = {}, form = {}) {
    let formData = {}
    Object.keys(files).forEach(k => {
      formData[k] = fs.createReadStream(files[k])
    })
    formData = Object.assign(formData, form)
    return await this.request(url, 'POST', {qs, formData})
  }

  // ---
  get cache () {
    if (this._cache) return this._cache

    if (this.app.cache) {
      this._cache = this.app.cache
      return this._cache
    }
  }
}
