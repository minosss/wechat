'use strict'

/*
JSSDK使用步骤
1. 绑定域名
2. 引入JS文件
3. 通过config接口注入权限验证配置
4. 通过ready接口处理成功验证
5. 通过error接口处理失败验证
*/

const crypto = require('crypto')
const Provider = require('./provider')

/**
 * Jssdk
 */
module.exports = class Jssdk extends Provider {
  get cacheKey () {
    if (!this._cacheKey) {
      const md5 = crypto.createHash('md5')
      this._cacheKey = 'wechat.cache.ticket_token.' + md5.update(JSON.stringify(this.credential)).digest('hex')
    }
    return this._cacheKey
  }

  get credential () {
    return {
      appid: this.app.config.appid,
      secret: this.app.config.secret
    }
  }

  /**
   * getToken
   */
  async getToken (refresh = false) {
    // cache
    if (!refresh && await this.cache.has(this.cacheKey)) {
      const token = this.cache.get(this.cacheKey)
      return token
    }

    const res = await this.httpGet('cgi-bin/ticket/getticket', {type: 'jsapi'})

    if (res.ticket) {
      this.cache.set(this.cacheKey, res.ticket, res.expires_in - 500)
      return res.ticket
    }

    throw new Error(`${res.errcode}: ${res.errmsg}`)
  }

  /**
   * signature 签名
   */
  signature (ticket, nonce, timestamp, url) {
    const sha1 = crypto.createHash('sha1')
    return sha1.update(`jsapi_ticket=${ticket}&noncestr=${nonce}&timestamp=${timestamp}&url=${url}`).digest('hex')
  }

  /**
   * config 获取配置
   */
  async config (apis, url, debug = false) {
    const {
      appid
    } = this.app.config
    const ticket = await this.getToken()
    const nonce = crypto.randomBytes(20).toString('hex')
    const timestamp = Math.floor(Date.now() / 1000)
    const signature = this.signature(ticket, nonce, timestamp, url)
    // -
    return {
      debug,
      appId: appid,
      timestamp,
      nonceStr: nonce,
      signature: signature,
      jsApiList: apis
    }
  }
}
