'use strict'

const request = require('request')

module.exports = async (options, async = true) => {
  if (!async) return request(options)
  return new Promise((resolve, reject) => {
    request(options, (err, response, body) => {
      if (err) {
        console.error(err)
        reject(err)
        return
      }
      // if (response.statusCode < 400)
      resolve(body)
    })
  })
}
