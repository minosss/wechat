'use strict'

const crypto = require('crypto')

exports.checkSignature = function (query, token) {
  const {
    signature,
    timestamp,
    nonce
  } = query

  let shasum = crypto.createHash('sha1')
  shasum.update([token, timestamp, nonce].sort().join(''))

  return shasum.digest('hex') === signature
}

const formatMessage = exports.formatMessage = function (result) {
  var message = {}
  if (typeof result === 'object') {
    for (var key in result) {
      if (!(result[key] instanceof Array) || result[key].length === 0) {
        continue
      }
      if (result[key].length === 1) {
        var val = result[key][0]
        if (typeof val === 'object') {
          message[key] = formatMessage(val)
        } else {
          message[key] = (val || '').trim()
        }
      } else {
        message[key] = []
        result[key].forEach(function (item) {
          message[key].push(formatMessage(item))
        })
      }
    }
    return message
  } else {
    return result
  }
}

const { Builder } = require('xml2js')
const builder = new Builder({cdata: true, rootName: 'xml'})
// const { Message } = require('./messages')

exports.buildXMLString = function (msg) {
  return builder.buildObject(msg)
}

// exports.rawToMessage = function () {}
