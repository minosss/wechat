# @wechat/core

> @wechat sdk core

## Install

Using npm:

```
npm i @wechat/core
```

or using yarn:

```
yarn add @wechat/core
```
