'use strict'

const { checkSignature, formatMessage } = require('@wechat/core/utils')
const { Provider: BaseProvider } = require('@wechat/core/providers')

/**
 * 服务号
 */
module.exports = class Application {
  constructor (config) {
    // 配置
    this.config = Object.assign({
      // 开启的功能服务
      // 处理handler外，其他需要access-token支持
      providers: [
        './providers/access-token',
        './providers/auto-reply',
        './providers/base',
        './providers/broadcasting',
        './providers/comment',
        './providers/handler',
        './providers/material',
        './providers/menu',
        './providers/oauth',
        './providers/qrcode',
        './providers/template-message',
        './providers/url',
        './providers/user',
        './providers/user-tag',
        // core
        '@wechat/core/providers/media'
      ]
    }, config)

    // 注册功能服务
    const {providers} = this.config
    for (let p of providers) {
      try {
        let Provider = require(p)
        if (Provider) {
          const provider = new Provider(this)
          if (provider instanceof BaseProvider) {
            // TODO: 换成用key或者放到各个provider处理
            this[p.split('/').pop()] = provider
            console.log('[provider] add %s', p)
          } else {
            throw new TypeError(`${p} is not a provider!!`)
          }
        }
      } catch (err) {
        console.error(err)
      }
    }
  }

  // TODO: 脱离koa
  middleware () {
    const wechat = this
    const { token } = this.config

    return async (ctx, next) => {
      ctx.state.wechat = wechat
      // check signature
      if (!checkSignature(ctx.query, token)) {
        console.log('fail')
        ctx.status = 401
        ctx.body = 'Invalid signature'
        return
      }
      // 设置接口配置信息时返回echostr
      if (ctx.method === 'GET') {
        console.log('success', ctx.query.echostr)
        ctx.body = ctx.query.echostr
      } else if (ctx.method === 'POST') {
        // 处理接受的消息
        const msg = formatMessage(ctx.request.body.xml)
        console.log('收到信息', msg)
        // ---
        const reply = await this.handler.handle(msg)
        // ---
        console.log('回复消息', reply)
        if (reply !== '') ctx.type = 'application/xml'
        ctx.body = reply
      } else {
        // 不支持其他方法
      }
    }
  }
}
