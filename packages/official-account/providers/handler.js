'use strict'

const {
  of,
  from,
  Subject
} = require('rxjs')
const {
  scan,
  filter,
  takeUntil,
  tap,
  catchError
} = require('rxjs/operators')
//
const {
  Message,
  Text
} = require('@wechat/core/messages')
const DEFAULT = '*'
// 支持的接受消息类型
const TYPES = ['text', 'image', 'voice', 'video', 'shortvideo', 'location', 'link', 'event']
const Provider = require('./provider')

/**
 * 服务端的信息处理
 * 用来处理从微信那边POST过来的数据，主要是由用户触发
 */
module.exports = class Handler extends Provider {
  constructor (app, accessToken) {
    super(app, accessToken)

    this.supported = [DEFAULT, ...TYPES]
    this.handlers = []

    // 针对类型的快捷方法
    // handler.text(() => {})
    for (const type of TYPES) {
      this[type] = (fn) => {
        return this.use(type, fn)
      }
    }
  }

  /**
   * _checkSignature 检查签名
   */
  _checkSignature () {}

  /**
   * handle 处理路由请求
   *
   * @param {Message} message
   */
  async handle (message) {
    // TODO get/post都统一处理，改为直接处理request和response
    return await this._handlePost(message)
  }

  /**
   * _handleGet 处理GET请求
   * 用来处理微信设置服务器地址的请求验证
   */
  _handleGet () {}

  /**
   * _headlePost 处理从微信POST过来的消息
   */
  async _handlePost (message) {
    const {
      FromUserName,
      ToUserName
    } = message

    // TODO 把原始数据转化成统一消息格式 Message
    const { MsgType: type } = message
    const final$ = new Subject()
    // --
    let replyMessage = await from(this.handlers).pipe(
      // 检测消息类型是否在支持的类型中
      tap(_ => {
        if (!this.supported.includes(type)) {
          throw new Error('babbfaf  not suppoted type..')
        }
      }),
      filter(h => h.type === type || h.type === DEFAULT),
      scan((msg, h) => {
        if (msg === false) {
          final$.next(true)
          final$.unsubscribe()
          return
        }
        return h.fn(msg)
      }, message),
      takeUntil(final$),
      filter(val => val !== false),
      catchError(err => of(`err: ${err.message}`))
    ).toPromise()

    // 返回字符串的话，封装成普通消息
    if (typeof replyMessage === 'string' && replyMessage !== '') {
      replyMessage = new Text({content: replyMessage})
    }

    // 检查回复消息类型
    if (replyMessage instanceof Message) {
      try {
        // toXMLString
        let data = replyMessage.transformToXmlString({
          ToUserName: FromUserName,
          FromUserName: ToUserName,
          CreateTime: Math.floor(Date.now() / 1000)
        })
        return data
      } catch (err) {
        console.error(err)
      }
    }
    // -
    return ''
  }

  /**
   * use 添加消息处理方法
   *
   * @param {string|Function} type
   * @param {Function|null} fn
   * @return {Handler} self
   */
  use (type, fn) {
    if (typeof type === 'function') {
      fn = type
      type = DEFAULT
    }

    if (typeof fn !== 'function') {
      throw new TypeError('处理器必须是个函数')
    }

    if (!this.supported.includes(type)) {
      throw new TypeError(`${type} 类型不支持`)
    }

    this.handlers.push({fn, type})
    return this
  }
}
