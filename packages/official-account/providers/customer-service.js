'use strict'

const Provider = require('./provider')

/**
 * 客服管理
 */
module.exports = class CustomerService extends Provider {
  /**
   * create 添加客服帐号
   */
  async create (account, nickname) {
    return await this.httpPostJson('customservice/kfaccount/add', {
      kf_account: account,
      nickname
    })
  }

  /**
   * update 设置客服信息
   */
  async update (account, nickname) {
    return await this.httpPostJson('customservice/kfaccount/update', {
      kf_account: account,
      nickname
    })
  }

  /**
   * delete 删除客服帐号
   */
  async delete (account) {
    return await this.httpPostJson('customservice/kfaccount/del', {
      kf_account: account
    })
  }

  /**
   * list 获取客服基本信息
   */
  async list () {
    return await this.httpGet('cgi-bin/customservice/getkflist')
  }

  /**
   * online 获取在线客服
   */
  async online () {
    return await this.httpGet('cgi-bin/customservice/getonlinekflist')
  }

  /**
   * invite 邀请绑定客服帐号
   */
  async invite (account, username) {
    return await this.httpPostJson('customservice/kfaccount/inviteworker', {
      kf_account: account,
      invite_wx: username
    })
  }

  /**
   * setAvatar 上传客服头像
   */
  async setAvatar (account, path) {
    return await this.httpUpload('customservice/kfaccount/uploadheadimg', {media: path}, {kf_account: account})
  }

  /**
   * messages 获取聊天记录
   */
  async messages (startTime, endTime, msgID = 1, number = 10000) {
    return await this.httpPostJson('customservice/msgrecord/getmsglist', {
      starttime: startTime,
      endtime: endTime,
      msgid: msgID,
      number
    })
  }
}
