'use strict'

const Provider = require('./provider')

/**
 * AuthReply 自动回复
 *
 * @see [获取公众号的自动回复规则](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751299)
 */
module.exports = class AutoReply extends Provider {
  /**
   * current 获取公众号的自动回复规则
   */
  async current () {
    return await this.httpGet('cgi-bin/get_current_autoreply_info')
  }
}
