'use strict'

const Provider = require('./provider')

/**
 * TemplateMessage 模板消息
 *
 * @see [模板消息运营规范](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751288)
 */
module.exports = class TemplateMessage extends Provider {
  /**
   * addTemplate 获得模板ID
   */
  async addTemplate (shortID) {
    return await this.httpPostJson('cgi-bin/template/api_add_template', {
      template_id_short: shortID
    })
  }

  /**
   * getAllPrivateTemplates 获取模板列表
   */
  async getAllPrivateTemplates () {
    return await this.httpGet('cgi-bin/template/get_all_private_template')
  }

  /**
   * deletePrivateTemplate 删除模板
   */
  async deletePrivateTemplate (templateID) {
    return await this.httpPostJson('cgi-bin/template/del_private_template', {
      template_id: templateID
    })
  }

  /**
   * send 发送模板消息
   */
  async send (message, to, miniprogram) {
    let msg = message.transformForJsonRequestWithoutType({
      touser: to,
      miniprogram
    })
    return await this.httpPostJson('cgi-bin/message/template/send', msg)
  }

  /**
   * sendSubscription TODO: 推送订阅模板消息给到授权微信用户
   * 一次性订阅消息
   */
  // async subscribe (data) {
  //   let msg = {}
  //   return await this.httpPostJson('cgi-bin/message/template/subscribe', msg)
  // }

  /**
   * getIndustry 获取设置的行业信息
   */
  async getIndustry () {
    return await this.httpGet('cgi-bin/template/get_industry')
  }

  /**
   * setIndustry 设置所属行业
   * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751277
   */
  async setIndustry (industryOne, industryTwo) {
    return await this.httpPostJson('cgi-bin/template/api_set_industry', {
      industry_id1: industryOne,
      industry_id2: industryTwo
    })
  }
}
