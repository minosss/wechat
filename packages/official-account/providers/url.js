'use strict'

const Provider = require('./provider')

/**
 * Url 短网址
 */
module.exports = class Url extends Provider {
  /**
   * shorten 获取短链接
   */
  async shorten (url) {
    return await this.httpPostJson('cgi-bin/shorturl', {
      action: 'long2short',
      long_url: url
    })
  }
}
