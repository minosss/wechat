'use strict'

const Provider = require('./provider')
const { Article } = require('@wechat/core/messages')

/**
 * material 在微信里的图片，音乐，视频等等都需要先上传到微信服务器作为素材才可以在消息中使用
 */
module.exports = class Material extends Provider {
  constructor (app, accessToken) {
    super(app, accessToken)
    this.allowTypes = ['image', 'voice', 'video', 'thumb', 'news_image']
  }

  /**
   * uploadImage 上传图片
   */
  async uploadImage (path) {
    return await this._upload('image', path)
  }

  /**
   * uploadVoice 上传音频
   */
  async uploadVoice (path) {
    return await this._upload('voice', path)
  }

  /**
   * uploadVideo 上传视频
   */
  async uploadVideo (path, title, description) {
    const form = {
      description: JSON.stringify({
        title: title,
        introduction: description
      })
    }
    return await this._upload('video', path, form)
  }

  /**
   * uploadThumb 上传缩略图
   */
  async uploadThumb (path) {
    return await this._upload('thumb', path)
  }

  /**
   * 用来统一处理上传，不要在外面调用这个方法
   */
  async _upload (type, media, form = {}) {
    if (!this.allowTypes.includes(type)) {
      throw new Error(`不支持媒体类型: ${type}`)
    }
    // NOTE: 微信这个SB
    const api = type === 'news_image' ? 'cgi-bin/media/uploadimg' : 'cgi-bin/material/add_material'
    return await this.httpUpload(api, {media}, form)
  }

  /**
   * uploadArticle 上传永久图片消息
   */
  async uploadArticle (articles) {
    if (!Array.isArray(articles)) {
      articles = [articles]
    }

    let data = {
      articles: articles.map(art => {
        if (art instanceof Article) {
          return art.transformForJsonRequestWithoutType()
        }
        return art
      })
    }

    return await this.httpPostJson('cgi-bin/material/add_news', data)
  }

  /**
   * uploadArticleImage 上传永久图文消息内的图片获取URL
   * 本接口所上传的图片不占用公众号的素材库中图片数量的5000个的限制。图片仅支持jpg/png格式，大小必须在1MB以下。
   */
  async uploadArticleImage (path) {
    return await this._upload('news_image', path)
  }

  /**
   * updateArticle 更新永久图文消息
   */
  async updateArticle (mediaID, article, index = 0) {
    if (article instanceof Article) {
      article = article.transformForJsonRequestWithoutType()
    } else if (Array.isArray(article)) {
      article = article[index] || {}
    }
    let data = {
      media_id: mediaID,
      index,
      articles: article
    }
    this.httpPostJson('cgi-bin/material/update_news', data)
  }

  /**
   * get 获取永久图片消息素材
   */
  get (mediaID) {
    // TODO: POST cgi-bin/material/get_material
    // -- 如果是视频，返回的是视频信息 --
  }

  /**
   * list 获取素材列表
   */
  async list (type, offset = 0, count = 20) {
    const data = {
      type,
      offset,
      count: Math.min(20, count)
    }
    return await this.httpPostJson('cgi-bin/material/batchget_material', data)
  }

  /**
   * stats 获取素材总数
   */
  async stats () {
    return await this.httpGet('cgi-bin/material/get_materialcount')
  }

  /**
   * delete 删除素材
   */
  async delete (mediaID) {
    return await this.httpPostJson('cgi-bin/material/del_material', {media_id: mediaID})
  }
}
