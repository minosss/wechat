'use strict'

const { Provider: BaseProvider } = require('@wechat/core/providers')

module.exports = class Provider extends BaseProvider {}
