'use strict'

const Provider = require('./provider')

/**
 * UserTag 用户标签管理
 * 开发者可以使用用户标签管理的相关接口，实现对公众号的标签进行创建、查询、修改、删除等操作，也可以对用户进行打标签、取消标签等操作。
 */
module.exports = class UserTag extends Provider {
  /**
   * create 创建标签
   * 一个公众号，最多可以创建100个标签。
   */
  async create (name) {
    return await this.httpPostJson('cgi-bin/tags/create', {
      tag: {name}
    })
  }

  /**
   * get 获取公众号已创建的标签
   */
  async get () {
    return await this.httpGet('cgi-bin/tags/get')
  }

  /**
   * 编辑标签
   */
  async update (id, name) {
    this.httpPostJson('cgi-bin/tags/update', {
      tag: {id, name}
    })
  }

  /**
   * delete 删除标签
   * 请注意，当某个标签下的粉丝超过10w时，后台不可直接删除标签。此时，开发者可以对该标签下的openid列表，先进行取消标签的操作，直到粉丝数不超过10w后，才可直接删除该标签。
   */
  async delete (id) {
    this.httpPostJson('cgi-bin/tags/delete', {
      tag: {id}
    })
  }
}
