'use strict'

const Provider = require('./provider')
const {
  Text
} = require('@wechat/core/messages')

/**
 * broadcasting 消息群发
 */
module.exports = class Broadcasting extends Provider {
  _build (message, to = null) {
    let content = message.transformForJsonRequest()

    let receive = {
      filter: {
        is_to_all: true
      }
    }
    if (typeof to === 'number') {
      receive = {
        filter: {
          is_to_all: false,
          tag_id: parseInt(to)
        }
      }
    } else if (Array.isArray(to) || typeof to === 'string') {
      receive = {
        touser: to
      }
    }

    message = Object.assign(content, receive)
    return message
  }
  /**
   * sendText 群发文本消息
   */
  async sendText (content, to = null) {
    return await this.sendMessage(new Text({content}), to)
  }

  // TODO: 发送其他类型消息

  /**
   * 群发Message类型消息
   */
  async sendMessage (message, to = null) {
    return await this.send(this._build(message, to))
  }

  /**
   * send 群发
   */
  async send (message) {
    if (!message.filter && !message.touser) {
      throw new Error('消息没有指定接收对象')
    }
    const api = message.touser ? 'cgi-bin/message/mass/send' : 'cgi-bin/message/mass/sendall'
    return await this.httpPostJson(api, message)
  }

  /**
   * delete 删除群发消息
   * 貌似不能删除文本之类的消息吧，只能删除图文消息
   */
  async delete (msgID, articleIndex = 0) {
    return await this.httpPostJson('cgi-bin/message/mass/delete', {
      msg_id: msgID,
      article_idx: articleIndex
    })
  }

  // preview start

  /**
   * previewText 预览文本消息
   */
  async previewText (content, to) {
    return await this.previewMessage(new Text({content}), to)
  }

  /**
   * previewMessage 预览消息，必须针对某个用户
   * 接口每天有限制
   */
  async previewMessage (message, to) {
    return await this.preview(this._build(message, to))
  }

  /**
   * preview
   */
  async preview (message) {
    if (!message.touser) {
      throw new Error('消息没有指定接收对象')
    }
    return await this.httpPostJson('cgi-bin/message/mass/preview', message)
  }

  // preview end

  /**
   * getSpeed 获取群发速度
   */
  async getSpeed () {
    return await this.httpPostJson('cgi-bin/message/mass/speed/get')
  }

  /**
   * setSpeed 设置群发速度
   */
  async setSpeed (speed) {
    return await this.httpPostJson('cgi-bin/message/mass/speed/set', {speed})
  }

  /**
   * status 查询群发消息发送状态
   */
  async status (msgID) {
    return await this.httpPostJson('cgi-bin/message/mass/get', {msg_id: msgID})
  }
}
