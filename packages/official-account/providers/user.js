'use strict'

const Provider = require('./provider')

/**
 * User 用户
 */
module.exports = class User extends Provider {
  /**
   * get 获取用户基本信息
   */
  async get (openid, lang = 'zh_CN') {
    return await this.httpGet('cgi-bin/user/info', {openid, lang})
  }

  /**
   * select 批量获取用户基本信息
   */
  async select (openids, lang = 'zh_CN') {
    return await this.httpPostJson('cgi-bin/user/info/batchget', {
      user_list: openids.map(openid => ({openid, lang}))
    })
  }

  /**
   * remark 设置用户备注名
   */
  async remark (openid, remark) {
    return await this.httpPostJson('cgi-bin/user/info/updateremark', {openid, remark})
  }

  /**
   * list 获取用户列表
   */
  async list (nextOpenID = null) {
    return await this.httpGet('cgi-bin/user/get', {next_openid: nextOpenID})
  }

  /**
   * list 获取标签下粉丝列表
   */
  async listByTag (tagID, nextOpenID = null) {
    this.httpPostJson('cgi-bin/user/tag/get', {
      tagid: tagID,
      next_openid: nextOpenID
    })
  }

  /**
   * block 拉黑用户
   */
  async block (openids) {
    return await this.httpPostJson('cgi-bin/tags/members/batchblacklist', {openid_list: openids})
  }

  /**
   * unblock 取消拉黑用户
   */
  async unblock (openids) {
    return await this.httpPostJson('cgi-bin/tags/members/batchunblacklist', {openid_list: openids})
  }

  /**
   * blacklist 获取公众号的黑名单列表
   */
  async blacklist (beginOpenID) {
    return await this.httpPostJson('cgi-bin/tags/members/getblacklist', {begin_openid: beginOpenID})
  }

  /**
   * tags 获取用户身上的标签列表
   */
  async tags (openid) {
    return await this.httpPostJson('cgi-bin/tags/getidlist', {
      openid
    })
  }

  /**
   * tag 批量为用户打标签
   */
  async tag (tagid, openids) {
    return await this.httpPostJson('cgi-bin/tags/members/batchtagging', {
      openid_list: openids,
      tagid
    })
  }

  /**
   * untag 批量为用户取消标签
   */
  async unag (tagid, openids) {
    return await this.httpPostJson('cgi-bin/tags/members/batchuntagging', {
      openid_list: openids,
      tagid
    })
  }
}
