'use strict'

const { Token } = require('@wechat/core/providers')

/**
 * 微信的访问令牌
 */
module.exports = class AccessToken extends Token {
  get credential () {
    return {
      grant_type: 'client_credential',
      appid: this.app.config.appid,
      secret: this.app.config.secret
    }
  }
}
