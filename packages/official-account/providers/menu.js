'use strict'

const Provider = require('./provider')

// 自定义菜单
module.exports = class Menu extends Provider {
  /**
   * create 添加菜单
   */
  async create (buttons, matchRule) {
    if (matchRule) {
      // 创建个性化菜单
      return await this.httpPostJson('cgi-bin/menu/addconditional', {button: buttons, matchrule: matchRule})
    }
    return await this.httpPostJson('cgi-bin/menu/create', {button: buttons})
  }

  /**
   * list 获取菜单列表
   */
  async list () {
    return await this.httpGet('cgi-bin/menu/get')
  }

  /**
   * current 获取当前列表
   */
  async current () {
    return await this.httpGet('cgi-bin/get_current_selfmenu_info')
  }

  /**
   * delete 删除菜单
   */
  async delete (menuID) {
    if (!menuID) return await this.httpGet('cgi-bin/menu/delete')

    // 删除个性化菜单
    return await this.httpPostJson('cgi-bin/menu/delconditional', {menuid: menuID})
  }

  /**
   * match 测试个性化菜单匹配结果
   */
  async match (userID) {
    return await this.httpPostJson('cgi-bin/menu/trymatch', {user_id: userID})
  }
}
