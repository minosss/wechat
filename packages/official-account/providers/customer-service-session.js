'use strict'

const Provider = require('./provider')

/**
 * CustomerServiceSession 客服会话控制
 */
module.exports = class CustomerServiceSession extends Provider {
  /**
   * open 创建会话
   */
  async open (account, openid) {
    return await this.httpPostJson('customservice/kfsession/create', {kf_account: account, openid})
  }

  /**
   * close 关闭会话
   */
  async close (account, openid) {
    return await this.httpPostJson('customservice/kfsession/close', {kf_account: account, openid})
  }

  /**
   * get 获取客户会话状态
   */
  async get (openid) {
    return await this.httpGet('customservice/kfsession/getsession', {openid})
  }

  /**
   * list 获取客服会话列表
   */
  async list (account) {
    return await this.httpGet('customservice/kfsession/getsessionlist', {kf_account: account})
  }

  /**
   * waitList 获取未接入会话列表
   */
  async waitList () {
    return await this.httpGet('customservice/kfsession/getwaitcase')
  }
}
