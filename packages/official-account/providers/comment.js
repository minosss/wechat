'use strict'

const Provider = require('./provider')

/**
 * Comment 图文消息留言管理接口
 */
module.exports = class Comment extends Provider {
  /**
   * open 打开已群发文章评论
   */
  async open (msgID, index) {
    return await this.httpPostJson('cgi-bin/comment/open', {
      msg_data_id: msgID,
      index
    })
  }

  /**
   * close 关闭已群发文章评论
   */
  async close (msgID, index) {
    return await this.httpPostJson('cgi-bin/comment/close', {
      msg_data_id: msgID,
      index
    })
  }

  /**
   * list 查看指定文章的评论数据
   */
  async list (msgID, index, begin, count, type = 0) {
    let body = {
      msg_data_id: msgID,
      index,
      begin,
      count: Math.min(count, 50),
      type
    }
    return await this.httpPostJson('cgi-bin/comment/list', body)
  }

  /**
   * markElect 将评论标记精选
   */
  async markElect (msgID, index, commentID) {
    return await this.httpPostJson('cgi-bin/comment/markelect', {
      msg_data_id: msgID,
      index,
      user_comment_id: commentID
    })
  }

  /**
   * unmarkElect 将评论取消精选
   */
  async unmarkElect (msgID, index, commentID) {
    return await this.httpPostJson('cgi-bin/comment/unmarkelect', {
      msg_data_id: msgID,
      index,
      user_comment_id: commentID
    })
  }

  /**
   * delete 删除评论
   */
  async delete (msgID, index, commentID) {
    return await this.httpPostJson('cgi-bin/comment/delete', {
      msg_data_id: msgID,
      index,
      user_comment_id: commentID
    })
  }

  /**
   * reply 回复评论
   */
  async reply (msgID, index, commentID, content) {
    return await this.httpPostJson('cgi-bin/comment/reply/add', {
      msg_data_id: msgID,
      index,
      user_comment_id: commentID,
      content
    })
  }

  /**
   * deleteReply 删除回复
   */
  async deleteReply (msgID, index, commentID) {
    return await this.httpPostJson('cgi-bin/comment/reply/delete', {
      msg_data_id: msgID,
      index,
      user_comment_id: commentID
    })
  }
}
