'use strict'

const Provider = require('./provider')

const DAY = 86400
const SCENE_MAX_VALUE = 100000
// const SCENE_QR_CARD = 'QR_CARD'
const SCENE_QR_TEMPORARY = 'QR_SCENE'
const SCENE_QR_TEMPORARY_STR = 'QR_STR_SCENE'
const SCENE_QR_FOREVER = 'QR_LIMIT_SCENE'
const SCENE_QR_FOREVER_STR = 'QR_LIMIT_STR_SCENE'

/**
 * QRCoode 二维码
 *
 * 临时二维码，是有过期时间的，最长可以设置为在二维码生成后的 30天后过期，但能够生成较多数量。临时二维码主要用于帐号绑定等不要求二维码永久保存的业务场景
 * 永久二维码，是无过期时间的，但数量较少（目前为最多10万个）。永久二维码主要用于适用于帐号绑定、用户来源统计等场景。
 */
module.exports = class QRCode extends Provider {
  /**
   * temporary 临时二维码
   *
   * {
   *  ticket,
   *  expire_seconds,
   *  url
   * }
   */
  async temporary (sceneValue, expireSeconds = null) {
    let type = SCENE_QR_TEMPORARY_STR
    let sceneKey = 'scene_str'

    if (/^[1-9]\d*$/.test(sceneValue) && sceneValue > 0) {
      type = SCENE_QR_TEMPORARY
      sceneKey = 'scene_id'
    }
    let scene = {
      [sceneKey]: sceneValue
    }
    return await this._create(type, scene, true, expireSeconds)
  }

  /**
   * forever 永久二维码
   *
   * {
   *  ticket,
   *  url
   * }
   */
  async forever (sceneValue) {
    let type = SCENE_QR_FOREVER_STR
    let sceneKey = 'scene_str'

    if (/^[1-9]\d*$/.test(sceneValue) && sceneValue > 0 && sceneValue < SCENE_MAX_VALUE) {
      type = SCENE_QR_FOREVER
      sceneKey = 'scene_id'
    }
    let scene = {
      [sceneKey]: sceneValue
    }

    return await this._create(type, scene, false)
  }

  /**
   * url 二维码网址
   */
  url (ticket) {
    return `https://api.weixin.qq.com/cgi-bin/showqrcode?ticket=${encodeURI(ticket)}`
  }

  async _create (actionName, actionInfo, temporary = true, expireSeconds = null) {
    if (!expireSeconds) expireSeconds = 7 * DAY
    let data = {
      action_name: actionName,
      action_info: {
        scene: actionInfo
      }
    }

    if (temporary) {
      data.expire_seconds = Math.min(expireSeconds, 30 * DAY)
    }

    return await this.httpPostJson('cgi-bin/qrcode/create', data)
  }
}
