'use strict'

const Provider = require('./provider')

module.exports = class Base extends Provider {
  /**
   * clearQuota 清理接口调用次数
   */
  async clearQuota () {
    const data = {
      appid: this.app.config.appid
    }
    return await this.httpPostJson('cgi-bin/clear_quota', data)
  }

  /**
   * getValidIPs 获取微信服务器 IP (或IP段)
   */
  async getValidIPs () {
    return await this.httpGet('cgi-bin/getcallbackip')
  }
}
