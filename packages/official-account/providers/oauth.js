'use strict'

const Provider = require('./provider')

const AUTH_URL = 'https://open.weixin.qq.com/connect/oauth2/authorize'
const TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/access_token'
const PROFILE_URL = 'https://api.weixin.qq.com/sns/userinfo'
const REFRESH_URL = 'https://api.weixin.qq.com/sns/oauth2/refresh_token'

exports.SCOPES_BASE = 'snsapi_base'
const SCOPES_USER = exports.SCOPES_USER = 'snsapi_userinfo'

/**
 * OAuth 网页授权
 *
 * @see [微信网页授权](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140842)
 */
module.exports = class OAuth extends Provider {
  /**
   * authorize 微信认证的链接
   */
  authorize (scopes, callbackURL, state = 'STATE') {
    // https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect
    const {
      appid
    } = this.app.config
    // redirect -> wechat
    return `${AUTH_URL}?appid=${appid}&redirect_uri=${callbackURL}&response_type=code&scope=${scopes}&state=${state}#wechat_redirect`
  }

  /**
   * callback 处理微信返回回来的code
   */
  async callback (code, lang = 'zh_CN') {
    // https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
    const {
      appid,
      secret
    } = this.app.config
    let uri = `${TOKEN_URL}?appid=${appid}&secret=${secret}&code=${code}&grant_type=authorization_code`
    const res = await this.httpClient({uri, json: true})

    if (!res.access_token) {
      // error
      return res
    }

    // fetch userinfo
    if (res.scope === SCOPES_USER) {
      uri = `${PROFILE_URL}?access_token=${res.access_token}&openid=${res.openid}&lang=${lang}`
      const user = await this.httpClient({uri, json: true})
      return user
    }
    return res
  }

  /**
   * refresh 刷新缓存
   * access_token只有7200秒的有效期，过期后可以使用refresh_token刷新
   */
  async refresh (refreshToken) {
    const {
      appid
    } = this.app.config
    const uri = `${REFRESH_URL}?appid=${appid}&grant_type=refresh_token&refresh_token=${refreshToken}`
    const res = await this.httpClient({uri, json: true})
    return res
  }
}
