# @wechat/official-account

> wechat official accounts

## Install

Using npm:

```
npm i @wechat/official-account
```

or using yarn:

```
yarn add @wechat/official-account
```
