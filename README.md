> @wechat/.

[![](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)
[![Standard - JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

写个SDK学学nodejs，灵感来自 [overture/wechat](https://github.com/overtrue/wechat)

## 功能

* [ ] 服务号
  * [x] 基础接口
  * [x] 服务端
  * [x] 菜单
  * [x] 消息
  * [x] 消息群发
  * [ ] 用户
  * [ ] 用户标签
  * [ ] JSSDK
  * [ ] 网页授权
  * [ ] 模板消息
  * [ ] 卡卷
  * [ ] 门店
  * [x] 自动回复
  * [ ] 客服
  * [x] 多客服消息转发
  * [x] 素材管理
  * [x] 临时素材
  * [x] 二维码
  * [ ] 门店
  * [x] 短网址
  * [x] 评论数据管理
* [ ] 小程序
* [ ] 支付

## 错误码

```
- 系统繁忙
0 成功
40003 - 传入非法的openid
40005 - 不支持的媒体类型
40009 - 媒体文件长度不合法
45009 - reach max api daily quota limit 没有剩余的调用次
45057 - 该标签下粉丝数超过10w，不允许直接删除
45058 - 不能修改0/1/2这三个系统默认保留的标签
45056 - 创建的标签数过多，请注意不能超过100个
45065 - clientmsgid exist 相同 clientmsgid 已存在群发记录，返回数据中带有已存在的群发任务的 msgid
45066 - 相同 clientmsgid 重试速度过快，请间隔1分钟重试
45067 - clientmsgid 长度超过限制
45083 - 设置的 speed 参数不在0到4的范围内
45084 - 没有设置 speed 参数
45157 - 标签名非法，请注意不能和其他标签重名
45158 - 标签名长度超过30个字节
45159 - 非法的tag_id
65400 - API不可用，即没有开通/升级到新版客服功能
65401 - 无效客服帐号
65403 - 客服昵称不合法
65404 - 客服帐号不合法
65405 - 帐号数目已达到上限，不能继续添加
65406 - 已经存在的客服帐号
65407 - 邀请对象已经是该公众号客服
65408 - 本公众号已经有一个邀请给该微信
65409 - 无效的微信号
65410 - 邀请对象绑定公众号客服数达到上限（目前每个微信号可以绑定5个公众号客服帐号）
65411 - 该帐号已经有一个等待确认的邀请，不能重复邀请
65412 - 该帐号已经绑定微信号，不能进行邀请

88000 - open comment without comment privilege 没有留言权限
88001 - msg_data is not exists 该图文不存在
88002 - the article is limit for safety 文章存在敏感信息
88003 - elected comment upper limit 精选评论数已达上限
88004 - comment was deleted by user 已被用户删除，无法精选
88005 - already reply 已经回复过了
88007 - reply content beyond max len or content len is zero 回复超过长度限制或为
88008 - comment is not exists 该评论不存
```

## License

MIT
